<?php
/*
 * Plugin Name: DX Events
 * Description: DX Events - is a solution to create and maintain events.
 * Author: Devrix
 * Text Domain: dx-events
*/

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

if( ! defined( 'DX_EV_DIR' ) ) {
	define( 'DX_EV_DIR', dirname( __FILE__ ) );
}

if ( ! class_exists( 'DX_Events' ) ) {
class DX_Events {

	/**
	 * Plugin assets base URL.
	 *
	 * @access protected
	 *
	 * @var string
	 */
	protected $assets_url;

	/**
	 * Default custom fields.
	 *
	 * @access public
	 *
	 * @var array
	 */
	public $custom_fields;

	function __construct() {
		// Register the events post type
		add_action( 'init', array( $this, 'register_post_type' ) );

		// Flush rewrite rules
		add_action( 'init', 'flush_rewrite_rules' );

		// Hook our plugins_loaded function
		add_action( 'plugins_loaded', array( $this, 'load_textdomain' ) );

		// Enqueue admin scripts
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );

		// Enqueue admin styles
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_styles' ) );

		// Enqueue scripts
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

		// Enqueue styles
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );

		// Add event metabox
		add_action( 'add_meta_boxes_dx_event', array( $this, 'add_event_metabox' ) );

		// Save custom fields data
		add_action( 'save_post', array( $this, 'save_custom_fields' ) );

		// Add custom class to meta container
		add_filter( "postbox_classes_dx_event_dx-event-options", array( $this, 'postbox_classes' ) );

		// Force WordPress to use our custom template for events post type archive
		add_filter( 'archive_template', array( $this, 'change_event_archive_template' ) );

		// Change events order by date
		add_filter( 'pre_get_posts', array( $this, 'change_events_order' ) );

		// Add Google Maps API field in Settings -> General
		add_action( 'admin_init' , array( $this , 'add_option_field' ) );

		// set assets URL
		$this->set_assets_url( plugins_url( '/assets/', __FILE__ ) );

		// Generate default custom fields
		$this->set_custom_fields();
	}

	/**
	 * Register events post type.
	 */
	public function register_post_type() {
		$labels = array(
			'name'               => __( 'Events', 'dx-events' ),
			'singular_name'      => __( 'Event', 'dx-events' ),
			'add_new'            => __( 'Add New', 'dx-events' ),
			'add_new_item'       => __( 'Add new Event', 'dx-events' ),
			'view_item'          => __( 'View Event', 'dx-events' ),
			'edit_item'          => __( 'Edit Event', 'dx-events' ),
			'new_item'           => __( 'New Event', 'dx-events' ),
			'view_item'          => __( 'View Event', 'dx-events' ),
			'search_items'       => __( 'Search Events', 'dx-events' ),
			'not_found'          => __( 'No Events found', 'dx-events' ),
			'not_found_in_trash' => __( 'No Events found in trash', 'dx-events' ),
		);

		register_post_type( 'dx_event', array(
			'labels'              => $labels,
			'public'              => true,
			'exclude_from_search' => false,
			'show_ui'             => true,
			'capability_type'     => 'post',
			'hierarchical'        => false,
			'_edit_link'          => 'post.php?post=%d',
			'query_var'           => true,
			'menu_icon'           => 'dashicons-calendar',
			'has_archive'         => true,
			'rewrite'             => array(
				'slug'       => 'event',
				'with_front' => true,
			),
			'supports'            => array(
				'title',
				'editor',
				'page-attributes',
				'thumbnail',
				'custom-fields',
			)
		));
	}

	/**
	 * Load language files
	 * @action plugins_loaded
	 */
	public function load_textdomain() {
		// initialize translations
		load_plugin_textdomain( 'dx-events', false, DX_EV_DIR . '/languages' );
	}

	/**
	 * Add custom metabox to event post type
	 *
	 * @param $post The post object
	 */
	public function add_event_metabox( $post ) {
		add_meta_box( 
			'dx-event-options',
			__( 'Event Options' ),
			array( $this, 'render_event_metabox' ),
			'dx_event',
			'normal',
			'default'
		);
	}

	/**
	 * Save custom fields
	 *
	 * @param $post_id The post ID
	 */
	function save_custom_fields( $post_id ) {
		// Add nonce for security and authentication.
		$nonce_name   = isset( $_POST['update-event'] ) ? $_POST['update-event'] : '';
		$nonce_action = 'update-event-meta';

		// Check if nonce is set.
		if ( ! isset( $nonce_name ) ) {
			return;
		}

		// Check if nonce is valid.
		if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) ) {
			return;
		}

		// Check if user has permissions to save data.
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}

		// Check if not an autosave.
		if ( wp_is_post_autosave( $post_id ) ) {
			return;
		}

		// Check if not a revision.
		if ( wp_is_post_revision( $post_id ) ) {
			return;
		}

		$fields = $this->get_custom_fields();

		foreach ($fields as $field => $placeholder) {
			$field_name = 'dx_event_' . $field;

			if ( isset( $_POST[ $field_name ] ) ) {
				update_post_meta( $post_id, $field_name, $_POST[ $field_name ] );
			}
		}
	}

	/**
	 * Output the markup
	 *
	 * @param $post The post object
	 */
	public function render_event_metabox( $post ) {
		include DX_EV_DIR . '/templates/dx-event-post-meta.php';
	}

	/**
	 * Enqueue main plugin scripts in administration.
	 *
	 * @access public
	 */
	public function admin_enqueue_scripts() {
		// Load the jQuery UI
		wp_enqueue_script( 'dx-events-admin-jquery-ui', 'http://code.jquery.com/ui/1.11.0/jquery-ui.min.js', array( 'jquery' ) );

		// Get the maps api key
		$api_key = $this->get_google_maps_api_key();

		// Load the Google Maps
		wp_enqueue_script( 'dx-events-admin-google-maps', '//maps.googleapis.com/maps/api/js?sensor=false&language=en&libraries=places' . ( $api_key ? '&key=' . $api_key : '' ) );

		// Load the jQuery Timepicker
		wp_enqueue_script( 'dx-events-admin-timepicker', $this->get_assets_url() . 'js/timepicker.js', array( 'jquery' ) );

		// Load the plugin javascript
		wp_enqueue_script( 'dx-events-admin-main-functions', $this->get_assets_url() . 'js/admin-main.js', array( 'jquery' ) );
	}

	/**
	 * Enqueue main plugin styles in administration.
	 *
	 * @access public
	 */
	public function admin_enqueue_styles() {
		// Load Jquery UI styles
		wp_enqueue_style( 'jquery-ui', $this->get_assets_url() . 'css/jquery-ui.min.css' );

		// Load the datepicker styles
		wp_enqueue_style( 'dx-events-datepicker', $this->get_assets_url() . 'css/datepicker.css' );

		// Load the plugin main styles
		wp_enqueue_style( 'dx-events-admin-main-styles', $this->get_assets_url() . 'css/admin-main.css' );
	}

	/**
	 * Enqueue main plugin scripts in front-end.
	 *
	 * @access public
	 */
	public function enqueue_scripts() {
		// Get the maps api key
		$api_key = $this->get_google_maps_api_key();

		// Load the Google Maps
		wp_enqueue_script( 'dx-events-google-maps', '//maps.googleapis.com/maps/api/js?sensor=false' . ( $api_key ? '&key=' . $api_key : '' ) );

		// Load the plugin javascript
		wp_enqueue_script( 'dx-events-main-functions', $this->get_assets_url() . 'js/main.js', array( 'jquery' ) );
	}

	/**
	 * Enqueue main plugin styles in front-end.
	 *
	 * @access public
	 */
	public function enqueue_styles() {
		// Load the plugin main styles
		wp_enqueue_style( 'dx-events-main-styles', $this->get_assets_url() . 'css/main.css' );
	}

	/**
	 * Retrieve the plugin assets base URL.
	 *
	 * @access public
	 *
	 * @return string $assets_url The plugin assets base URL.
	 */
	public function get_assets_url() {
		return $this->assets_url;
	}

	/**
	 * Modify the plugin assets base URL.
	 *
	 * @access protected
	 *
	 * @param string $assets_url The new plugin assets base URL.
	 */
	protected function set_assets_url( $assets_url ) {
		$this->assets_url = $assets_url;
	}

	/**
	 * Retrieve the plugin custom fields.
	 *
	 * @access public
	 *
	 * @return arrya $custom_fields The event custom fields.
	 */
	public function get_custom_fields() {
		return apply_filters( 'dx_event_custom_fields', $this->custom_fields );
	}

	/**
	 * Set the event main custom fields.
	 *
	 * @access public
	 */
	public function set_custom_fields( ) {
		$fields = array(
			'location'     => 'Event Location',
			'location_lat' => 'Event Location Lat',
			'location_lng' => 'Event Location Lng',
			'url'          => 'Event URL',
			'start_date'   => 'Event Start Date',
			'end_date'     => 'Event End Date',
		);

		$this->custom_fields = $fields;
	}

	/**
	 * Modify meta box classess
	 *
	 * @param $classes The container classes
	 */
	public function postbox_classes( $classes ) {
		$classes[] = 'dx-box';

		return $classes;
	}

	/**
	 * Modify the event post type archive template
	 *
	 * @param $archive_template The post type arhive template
	 */
	public function change_event_archive_template( $archive_template ) {
		if ( is_post_type_archive ( 'dx_event' ) ) {
			$archive_template = DX_EV_DIR . '/events-archive.php';
		}

		return $archive_template;
	}

	/**
	 * Change the events order by date ASC
	 *
	 * @param $query WP_Query obejct
	 */
	public function change_events_order( $query ) {
		if (
			$query->is_post_type_archive( 'dx_event' ) &&
			! is_admin()
		) {
			$meta_query = array(
			    array(
					'key'     => 'dx_event_end_date',
					'value'   => date( 'Y-m-d H:m:s' ),
					'compare' => '>=',
			    )
			);

			$query->set( 'meta_query', $meta_query );
			$query->set( 'orderby', 'meta_value' );
			$query->set( 'meta_key', 'dx_event_start_date' );
			$query->set( 'order', 'ASC' );
		}
	}

	/**
	 * Add maps API key in Settings->General
	 */
	public function add_option_field() {
		register_setting( 'general', 'dx_events_api_key', 'esc_attr' );
		add_settings_field(
			'dx_events_api_key',
			'<label for="dx_events_api_key">' . esc_html__( 'Google Maps API key' , 'dx_events_api_key' ) . '</label>',
			array( $this, 'option_field_api_key' ),
			'general'
		);
	}

	/**
	 * HTML for Google Maps field
	 */
	public function option_field_api_key() {
		// Check for value
		$value = get_option( 'dx_events_api_key', false );

		// Display the field
		echo '<input type="text" id="dx_events_api_key" name="dx_events_api_key" class="regular-text" value="' . esc_attr( $value ) . '" />';
	}

	public function get_google_maps_api_key() {
		$api_key = get_option( 'dx_events_api_key', false );

		return apply_filters( 'dx_events_api_key', $api_key );
	}

}
}
new DX_Events();
