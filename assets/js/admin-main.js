;(function($, window, document, undefined) {
	// Fire on document ready.
	$( document ).ready( function() {
		// Initialize the datepicker
		var startDate = $( '#dx_event_start_date' );
		var endDate   = $( '#dx_event_end_date' );

		startDate.datetimepicker({
			minDate: 0,
			dateFormat: 'yy-mm-dd',
			timeFormat: 'HH:mm:ss',
			onClose: function( dateText, inst ) {
				if ( endDate.val() !== '' ) {
					var testStartDate = startDate.datetimepicker( 'getDate' );
					var testEndDate   = endDate.datetimepicker( 'getDate' );

					if ( testStartDate > testEndDate ) {
						endDate.datetimepicker( 'setDate', testStartDate );
					}
				}
				else {
					endDate.val( dateText );
				}
			},
			onSelect: function ( selectedDateTime ){
				endDate.datetimepicker( 'option', 'minDate', startDate.datetimepicker('getDate') );
			}
		});

		endDate.datetimepicker({
			dateFormat: 'yy-mm-dd',
			timeFormat: 'HH:mm:ss',
		});
	})

	function initAutocomplete() {
		var lat = parseFloat( $( '#dx_event_location_lat' ).val() );
		var lng = parseFloat( $(' #dx_event_location_lng' ).val() );
		if ( ! $( lat ).length ) {
			lat = 40.730610;
		}

		if ( ! $( lng ).length ) {
			lng = -73.935242;
		}

		var latLng = new google.maps.LatLng( lat, lng );

		var map = new google.maps.Map( document.getElementById( 'map' ), {
			center: latLng,
			zoom: 17,
			mapTypeId: 'roadmap'
		});

		// Create the search box and link it to the UI element.
		var input = document.getElementById( 'dx_event_location' );
		var searchBox = new google.maps.places.SearchBox( input );
		map.controls[ google.maps.ControlPosition.TOP_LEFT ].push( input );

		google.maps.event.addDomListener( input, 'keydown', function ( e ) {
			if ( e.keyCode == 13 ) {
				e.preventDefault();
			}
		});

		// Bias the SearchBox results towards current map's viewport.
		map.addListener( 'bounds_changed', function () {
			searchBox.setBounds( map.getBounds() );
		});

		var markers = [];
		// Listen for the event fired when the user selects a prediction and retrieve
		// more details for that place.
		searchBox.addListener( 'places_changed', function () {
			var places = searchBox.getPlaces();

			if ( places.length == 0 ) {
				alert(' The place has not been found! ')
			}

			$( '#dx_event_location_lat' ).val( places[0].geometry.location.lat() );
			$( '#dx_event_location_lng' ).val( places[0].geometry.location.lng() );
			// $('#dx_event_location').val( places[0].formatted_address );

			// Clear out the old markers.
			markers.forEach( function( marker ) {
				marker.setMap( null );
			});

			markers = [];

			// For each place, get the icon, name and location.
			var bounds = new google.maps.LatLngBounds();

			places.forEach( function( place ) {
				if ( ! place.geometry ) {
					console.log( 'Returned place contains no geometry' );
					return;
				}

				// Create a marker for each place.
				markers.push( new google.maps.Marker({
					map: map,
					position: place.geometry.location
				}));

				if ( place.geometry.viewport ) {
					// Only geocodes have viewport.
					bounds.union( place.geometry.viewport );
				} else {
					bounds.extend( place.geometry.location );
				}
			});
			map.fitBounds( bounds );
		});

		markers.push( new google.maps.Marker({
			map: map,
			position: latLng
		}));
	}

	google.maps.event.addDomListener( window, 'load', initAutocomplete );
})(jQuery, window, document);
