;(function($, window, document, undefined) {
	// Fire on document ready.
	$( document ).ready( function() {
		google.maps.event.addDomListener( window, 'load', initMap );
	});
	function initMap () {
		$( '.map' ).each( function ( index ) {
			var selector = $( this ).data( 'id' )
			var lng      = $( '#' + selector ).data( 'lng' );
			var lat      = $( '#' + selector ).data( 'lat' );
			var latLng   = new google.maps.LatLng( lat, lng );

			var map = new google.maps.Map( document.getElementById( selector ), {
				zoom: 14,
				disableDefaultUI: true,
				center: latLng
			});

			var marker = new google.maps.Marker({
				position: latLng,
				map: map
			});
		})
	}
})(jQuery, window, document);
