<?php
/**
 * The template for displaying DX event archive
 */
get_header();
?>

<div class="wrap">

	<?php if ( have_posts() ) : ?>
		<header class="page-header">
			<?php
				the_archive_title( '<h1 class="page-title">', '</h1>' );
				the_archive_description( '<div class="taxonomy-description">', '</div>' );
			?>
		</header><!-- .page-header -->
	<?php endif; ?>

	<div class="shell">
		<div class="dx-events-main-content">

			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php
					$start_date = get_post_meta( $post->ID, 'dx_event_start_date', true );
					$end_date   = get_post_meta( $post->ID, 'dx_event_end_date', true );
					$location   = get_post_meta( $post->ID, 'dx_event_location', true );
					$lat        = get_post_meta( $post->ID, 'dx_event_location_lat', true );
					$lng        = get_post_meta( $post->ID, 'dx_event_location_lng', true );
					$url        = get_post_meta( $post->ID, 'dx_event_url', true );
					?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

						<header class="entry-header">
							<?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>
						</header><!-- .entry-header -->

						<div class="event-meta">
							<div class="event-meta">
								<?php if ( !empty( $url ) ): ?>
									<p>
										<a href="<?php echo esc_url( $url ); ?>" target="_blank">
											<?php echo esc_html_e( 'Visit Event Site', 'dx-events' ); ?>
										</a>
									</p>
								<?php endif ?>

								<?php
								if ( ! empty( $start_date ) && !empty( $end_date ) ) :
									// Convert dates to timestamp
									$start_date = strtotime( $start_date );
									$end_date   = strtotime( $end_date );

									// Create formatted date that will be displayed
									$formatted_start_date = date( 'j F, Y H:m a', $start_date );
									$formatted_end_date = date( 'j F, Y H:m a', $end_date );

									// Display Start date
									echo wpautop( esc_html__( sprintf( 'Start Date: %s', $formatted_start_date ), 'dx-events' ) );

									// Display End date
									echo wpautop( esc_html__( sprintf( 'End Date: %s', $formatted_end_date ), 'dx-events' ) );

									// Build date string for Google Calendar
									$formatted_date = date( 'Ymd\THms\Z', $start_date ) . '/' . date( 'Ymd\THms\Z', $end_date ) ;

									// Google Calendar link args
									$args = array(
										'action'   =>'TEMPLATE',
										'text'     => $post->post_title,
										'dates'    => $formatted_date,
									);

									// Add the location if have such
									if ( ! empty( $location ) ) {
										$args['location'] = $location;
									}

									// Add the URL if have such
									if ( !empty( $url ) ) {
										$args['details'] = 'For more details visit: ' . $url;
									}

									// Build the link.
									$link = add_query_arg(
										$args,
										'https://www.google.com/calendar/render'
									);
								?>
									<p>
										<a href="<?php echo esc_url( $link ); ?>" target="_blank">
											<?php echo esc_html_e( 'Add to Calendar', 'dx-events' ); ?>
										</a>
									</p>
								<?php endif ?>
							</div><!-- /.event-meta -->
						</div><!-- /.event-meta -->

						<?php if ( has_post_thumbnail() ): ?>
							<div class="post-thumbnail">
								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail( 'dx-event-image' ); ?>
								</a>
							</div><!-- .post-thumbnail -->
						<?php endif; ?>

						<div class="entry-content">
							<?php echo wp_trim_words( $post->post_content, 55, '...' ); ?>
						</div><!-- .entry-content -->

						<div
							class="map"
							data-lat="<?php echo esc_attr( $lat ); ?>"
							data-lng="<?php echo esc_attr( $lng ); ?>"
							data-id="map-<?php echo $post->ID ?>"
							id="map-<?php echo $post->ID ?>"
						></div><!-- /.map -->

					</article><!-- #post-## -->
				<?php endwhile; ?>

			<?php
			the_posts_pagination( array(
				'mid_size'  => 2,
				'prev_text' => __( 'Prev', 'dx-events' ),
				'next_text' => __( 'Next', 'dx-events' ),
			) );
			?>

			<?php else : ?>
				<p>
					<?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'twentyseventeen' ); ?>
				</p>
				<?php get_search_form(); ?>
			<?php endif; ?>
		</div><!-- .main-content -->

	</div><!-- /.shell -->

</div><!-- .wrap -->

<?php
get_footer();
