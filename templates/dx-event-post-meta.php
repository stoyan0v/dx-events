<?php
$custom_fields = $this->get_custom_fields();

if ( empty( $custom_fields ) ) {
	return;
}

wp_nonce_field( 'update-event-meta', 'update-event' );

// Build custom field inputs
foreach ($custom_fields as $field => $placeholder) :
	$field_name = 'dx_event_' . $field;
	$class_name = 'dx-field ' . $field_name . '-field';

	$value = get_post_meta( $post->ID, $field_name, true );
?>

	<div class="<?php echo $class_name ?>">
		<label for="<?php echo esc_attr( $field_name ); ?>">
			<?php echo esc_html_e( sprintf( '%s', $placeholder ), 'dx-events' ); ?>
		</label>

		<div class="field-holder">
			<input
				type="text"
				name="<?php echo esc_attr( $field_name ); ?>"
				id="<?php echo esc_attr( $field_name ); ?>"
				placeholder="<?php esc_attr_e( sprintf( '%s', $placeholder ), 'dx-events' ); ?>"
				value="<?php echo esc_attr( $value ) ?>"
			/>

			<?php if ( $field == 'location' ): ?>
			    <div id="map"></div>
			<?php endif ?>
		</div>
	</div>
<?php
endforeach;
